// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
// renderer.js
const {ipcRenderer} = require('electron');

window.addEventListener("DOMContentLoaded", () => {

    const waiterLogin = document.getElementById("login-button-waiter");
    const barmenLogin=document.getElementById("login-button-barmen");
    const managerLogin = document.getElementById("login-button-manager");

    const openWaiterSale=document.getElementById("btn-sale");
    const openWaiterSostojbi = document.getElementById("btn-sostojbi");
    const goBackFromWaiterLogin = document.getElementById("btn-back");
    const goBackFromWaiterSale=document.getElementById("btn-waiter-back");
    const waiterSaleTransferRight=document.getElementById("btn-waiter-transfer-right");
    const waiterSaleDelete=document.getElementById("btn-waiter-delete");
    const waiterSalePrint=document.getElementById("btn-waiter-print");
    const waiterSostojbiShowSold = document.getElementById("btn-show-sold");
    const waiterSostojbiShowDeleted=document.getElementById("btn-show-deleted");
    const waiterSostojbiShowBill=document.getElementById("btn-show-bill");
    const waiterSostobjiShowSale=document.getElementById("btn-show-sale");
    const waiterSostojbiPrintBill=document.getElementById("btn-print-bill");
    const goBackFromWaiterSostojbi = document.getElementById("btn-sostojbi-back");

    const openBarmenSale = document.getElementById("btn-barmen-sale");
    const goBackFromBarmenLogin = document.getElementById("btn-barmen-back");
    const barmenSaleDone=document.getElementById("btn-barmen-sale-done");
    const goBackFromBarmenSale=document.getElementById("btn-barmen-sale-back");

    const openManagerEmployees=document.getElementById("btn-manager-employees");
    const openManagerPopis = document.getElementById("btn-manager-popis");
    const goBackFromManagerLogin=document.getElementById("btn-manager-back");
    const managerSifrarnikSubmit=document.getElementById("btn-sifrarnik-submit");
    const goBackFromManagerSifrarnik = document.getElementById("btn-sifrarnik-back");
    const managerEmployeesSubmit=document.getElementById("btn-employees-submit");
    const managerEmployeesDelete = document.getElementById("btn-employees-delete");
    const goBackFromManagerEmployees = document.getElementById("btn-employees-back");
    const managerPopisPrint = document.getElementById("btn-print-popis");
    const goBackFromManagerPopis = document.getElementById("btn-popis-back");

    
    if (waiterLogin != null){
      waiterLogin.addEventListener("click", () => {
        document.location.href="./html-forms/waiter-login-popup.html";
      });
    }

    if(barmenLogin != null){
      barmenLogin.addEventListener("click", () => {
        document.location.href="./html-forms/barmen-login-popup.html";
      })
    }

    if (managerLogin != null){
      managerLogin.addEventListener("click", () => {
        document.location.href="./html-forms/manager-login-popup.html";
      })
    }

    if (goBackFromWaiterLogin != null){
      goBackFromWaiterLogin.addEventListener("click", () =>{
        document.location.href="../index.html";
      })
    }

    if(goBackFromBarmenLogin != null){
      goBackFromBarmenLogin.addEventListener("click", () =>{
        document.location.href="../index.html";
      })
    }

    if(goBackFromManagerLogin != null){
      goBackFromManagerLogin.addEventListener("click", () =>{
        document.location.href="../index.html";
      })
    }

    if(goBackFromWaiterSale !=null){
      goBackFromWaiterSale.addEventListener("click", () => {
        document.location.href="./waiter-login-popup.html";
      })
    }

    if(goBackFromWaiterSostojbi != null){
      goBackFromWaiterSostojbi.addEventListener("click", () =>{
        document.location.href="./waiter-login-popup.html";
      })
    }

    if(goBackFromBarmenSale != null){
      goBackFromBarmenSale.addEventListener("click", () => {
        document.location.href="./barmen-login-popup.html";
      })
    }

    if (goBackFromManagerSifrarnik != null){
      goBackFromManagerSifrarnik.addEventListener("click", () => {
        document.location.href="./manager-login-popup.html";
      })
    }

    if(goBackFromManagerEmployees != null){
      goBackFromManagerEmployees.addEventListener("click", () => {
        document.location.href="./manager-login-popup.html";
      })
    }

    if(goBackFromManagerPopis != null){
      goBackFromManagerPopis.addEventListener("click", () => {
        document.location.href="./manager-login-popup.html";
      })
    }

  });
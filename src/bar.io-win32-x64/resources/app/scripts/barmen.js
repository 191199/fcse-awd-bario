var Datastore = require('nedb');

var employees=new Datastore({filename: './databases/employees.db', autoload: true, corruptAlertThreshold: 1});
var products=new Datastore({filename: "./databases/products.db", autoload: true, corruptAlertThreshold: 1});
var tables=new Datastore({filename: "./databases/tables.db", autoload: true, corruptAlertThreshold: 1});
var sostojbi = new Datastore({filename: "./databases/sostojbi.db", autoload: true, corruptAlertThreshold: 1})

function generateTables(){
    $('#tables option').remove();
    tables.find({}, function(err, docs){
        if(!docs[0]){
            console.log("GRESKA");
        }else{
            var listOfTables=[]
            for(var i =0;i<docs.length;i++){
                listOfTables.push(docs[i].tableName)
            }
            var newSelect=document.getElementById('tables');
            var opt = document.createElement("option");
            opt.value= "";
            opt.innerHTML = ""; 
            newSelect.appendChild(opt);
            console.log(listOfTables)
            for(var i=0;i<listOfTables.length;i++){
                var opt = document.createElement("option");
                opt.value= listOfTables[i];
                opt.innerHTML = listOfTables[i]; 
                newSelect.appendChild(opt);
            }
        }
    });
}

function tableSelectChanged(){
    var selectedTable;
    $("#tables").find(":selected").each(function () {
        selectedTable= $(this).val();
    });

    resetTableToBeServed(selectedTable);

}


function resetTableToBeServed(tableNameToBeServed){
    $("#tableToBeServed tbody").empty();
    $('#tableToBeServed tr:gt(0)').remove();

    tables.find({tableName: tableNameToBeServed}, function(err, docs){
        if(!docs[0]){
            console.log("GRESKA");
        }else{
            var theadRef = document.getElementById('tableToBeServed').getElementsByTagName('thead')[0];
            var newRow = theadRef.insertRow(theadRef.rows.length);
            newRow.innerHTML = "<tr><td colspan=3>"+tableNameToBeServed+"</td></tr>";
            
            var productsInOrder=docs[0].tableProducts;
            if(productsInOrder.length > 0){
                for(var i = 0;i<productsInOrder.length;i++){
                    var imee = productsInOrder[i].ime;
                    var kolicinaa=productsInOrder[i].kolicina;
                    var cenaa=productsInOrder[i].cena;
                    var tbodyRef = document.getElementById('tableToBeServed').getElementsByTagName('tbody')[0];
                    var newRow = tbodyRef.insertRow(tbodyRef.rows.length);
                    newRow.innerHTML = "<tr><td>"+imee+"</td><td>"+kolicinaa+"</td><td>"+cenaa+"</td></tr>";

                }
            }
        }
    })
}


window.addEventListener("DOMContentLoaded", () => {
    const openBarmenSale=document.getElementById("btn-barmen-sale");
    const barmenInputPassword=document.getElementById("barmen-password");

    if(openBarmenSale != null){
        openBarmenSale.addEventListener("click", () => {
            if (barmenInputPassword.value != ""){
                employees.find({password: barmenInputPassword.value}, function(err, docs){
                    if(docs[0].type=="MANAGER"||docs[0].type=="BARMEN"){
                        document.location.href="../html-forms/barmen-sale.html";
                    }else{
                        console.log("GRESKA");
                    }
                });
            }
        });
    }

    generateTables();
    
    
    
});
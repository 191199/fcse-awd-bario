var Datastore = require('nedb');

var employees=new Datastore({filename: './databases/employees.db', autoload: true, corruptAlertThreshold: 1});
var products=new Datastore({filename: "./databases/products.db", autoload: true, corruptAlertThreshold: 1});
var sostojbi=new Datastore({filename: "./databases/sostojbi.db", autoload: true, corruptAlertThreshold: 1});


function convertFromStringToDateFrom(dateFrom, timeFrom) {
    let dateFromPieces = dateFrom.split("-");
    let timeFromPieces = timeFrom.split(":");
    return(new Date(dateFromPieces[0], (dateFromPieces[1] - 1), dateFromPieces[1],
                         timeFromPieces[0], timeFromPieces[1], '00'))
}
function convertFromStringToDateTo(dateTo, timeTo) {
    let dateToPieces = dateTo.split("-");
    let timeToPieces = timeTo.split(":");
    return(new Date(dateToPieces[0], (dateToPieces[1] - 1), dateToPieces[2],
                         timeToPieces[0], timeToPieces[1], '59'))
}

function convertDateEnded(dateEnded){
    let datePieces=dateEnded.split(" ");
    let date=datePieces[0].split("-");
    let time=datePieces[1].split(":");

    return(new Date(date[0], (date[1] - 1), date[2],
                         time[0], time[1], time[2]))
}

function printPopisTable(mapOfProducts){
    $("#popisTable tbody").empty();
    var keys=Array.from(mapOfProducts.keys());
    for(var key of keys){
        var ime=key;
        var kolicina=parseInt(mapOfProducts.get(key));
        var tbodyRef = document.getElementById('popisTable').getElementsByTagName('tbody')[0];
        var newRow = tbodyRef.insertRow(tbodyRef.rows.length);
        newRow.innerHTML = "<tr><td>"+ime+"</td><td>"+kolicina+"</td>";
    }
}


function export2Word(element){
    var preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>Popis To Doc</title></head><body>";
    var postHtml = "</body></html>";
    var html = preHtml+document.getElementById(element).innerHTML+postHtml;

    var blob = new Blob(['\ufeff', html], {
        type: 'application/msword'
    });
    
    var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
    
    filename = 'popis.docx';
    
    var downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob ){
        navigator.msSaveOrOpenBlob(blob, filename);
    }else{
        downloadLink.href = url;
        
        downloadLink.download = filename;
        
        downloadLink.click();
    }
    
    document.body.removeChild(downloadLink);
}

window.addEventListener("DOMContentLoaded", () =>{
    //sifrarnik
    const openManagerSifrarnik=document.getElementById("btn-manager-sifrarnik");
    const inputManagerPassword=document.getElementById("manager-password");
    const productNumberInput = document.getElementById("sifrarnik-product-number-input");
    const productCodeInput = document.getElementById("sifrarnik-product-code-input");
    const productNameInput = document.getElementById("sifrarnik-product-name-input");
    const productQuantityInput = document.getElementById("sifrarnik-product-quantity-input");
    const productPriceInput=document.getElementById("sifrarnik-product-price-input");
    const submitNewProduct = document.getElementById("btn-sifrarnik-submit");

    //employees
    const openManagerEmployees = document.getElementById("btn-manager-employees");
    const nameOfEmployee = document.getElementById("employees-name-input");
    const codeOfEmployee = document.getElementById("employees-code-input");
    const radioButtonsForEmployee = document.getElementsByName("emplooyes-choice");
    const submitNewEmployee = document.getElementById("btn-employees-submit");
    const deleteEmployee = document.getElementById("btn-employees-delete");
    var checkedEmployee="";

    //popis
    const openManagerPopis = document.getElementById("btn-manager-popis");
    const showManagerPopis=document.getElementById("btn-popis-show");
    const dateFromInput=document.getElementById("popis-date-from-picker");
    const timeFromInput=document.getElementById("popis-time-from-picker");
    const dateToInput=document.getElementById("popis-date-to-picker");
    const timeToInput=document.getElementById("popis-time-to-picker");
    const codeInput=document.getElementById("popis-waiter-code");

    var bodyToBeClicked1;

    products.find({}, function(err, docs){
        if(!docs[0]){
            console.log("GRESKA");
        }else{
            for(var i=0;i<docs.length;i++){
                var redenBroj = docs[i].productNumber;
                var kod = docs[i].productCode;
                var imee=docs[i].productName;
                var kolicina=docs[i].productQuantity;
                var cenaa=docs[i].productPrice;
                var tbodyRef = document.getElementById('sifrarnikTable').getElementsByTagName('tbody')[0];
                var newRow = tbodyRef.insertRow(tbodyRef.rows.length);
                newRow.innerHTML = "<tr><td>"+redenBroj+"</td><td>"+kod+"</td><td>"+imee+"</td><td>"+kolicina+"</td><td>"+cenaa+"</td></tr>";
            }
            bodyToBeClicked1=document.getElementById("sifrarnikTable").tBodies[0];
            $(bodyToBeClicked1).on('click','tr', function(){
                $(this).addClass("sifrarnik-selected").siblings().removeClass("sifrarnik-selected");
                selectedSifrarnik=$(this);
            })
        }
    });

    if(openManagerSifrarnik != null){
        openManagerSifrarnik.addEventListener("click", () =>{ 
            employees.find({password: inputManagerPassword.value}, function (err, docs){   
                console.log(docs[0].password);
                if(docs[0].password==inputManagerPassword.value){
                    document.location.href="../html-forms/sifrarnik.html";
                }else{
                    console.log("GRESKA");
                }
            });
        });
    }

    var selectedEmployee;
    var bodyToBeClicked;

    employees.find({}, function(err, docs){
        if(!docs[0]){
            console.log("GRESKA");
        }else{
            for(var i=0;i<docs.length;i++){
                var ime = docs[i].name;
                var type = docs[i].type;
                if(document.getElementById('employeesTable') != null){
                    var tbodyRef = document.getElementById('employeesTable').getElementsByTagName('tbody')[0];
                    var newRow = tbodyRef.insertRow(tbodyRef.rows.length);
                    newRow.innerHTML = "<tr><td colspan=10>"+ime+"</td><td colspan=10>"+type+"</td></tr>";
                }
            }
                if(document.getElementById('employeesTable') != null){
                bodyToBeClicked=document.getElementById("employeesTable").tBodies[0];
                console.log(bodyToBeClicked);
                $(bodyToBeClicked).on('click','tr', function(){
                    $(this).addClass("employee-selected").siblings().removeClass("employee-selected");
                    selectedEmployee=$(this);
                    console.log(selectedEmployee);
                })
            }
        }
    });

    if(openManagerEmployees != null){
        openManagerEmployees.addEventListener("click", () =>{  
            employees.find({password: inputManagerPassword.value}, function (err, docs){   
                console.log(docs[0].password);
                if(docs[0].password==inputManagerPassword.value){
                    document.location.href="../html-forms/employees.html";
                }else{
                    console.log("GRESKA");
                }
            });
        });
    }

    if(openManagerPopis != null){
        openManagerPopis.addEventListener("click", () =>{  
            employees.find({password: inputManagerPassword.value}, function (err, docs){   
                console.log(docs[0].password);
                if(docs[0].password==inputManagerPassword.value){
                    document.location.href="../html-forms/popis.html";
                }else{
                    console.log("GRESKA");
                }
            });
        });
    }

    if(submitNewEmployee != null){
        submitNewEmployee.addEventListener("click", () => {
            for(var i=0;i<radioButtonsForEmployee.length;i++){
                if(radioButtonsForEmployee[i].checked){
                    checkedEmployee = radioButtonsForEmployee[i].value;
                    console.log(checkedEmployee);
                }
            }
            if(nameOfEmployee.value=="" || codeOfEmployee.value=="" || checkedEmployee==""){
                console.log("GRESKA");
            }else{
                var employee = {
                    name: nameOfEmployee.value,
                    password: codeOfEmployee.value,
                    type: checkedEmployee
                }
                employees.insert(employee, function(err, docs){
                    console.log(docs);
                    window.location.reload();
                });
            }
        });
    }

    if(deleteEmployee != null){
        deleteEmployee.addEventListener("click", () => {
            var selectedEmployeeName= $(selectedEmployee).find('td:nth-child(1)').html();
            
            employees.remove({name: selectedEmployeeName}, {}, function(err, numRemoved){

            });

            let selectedInputs = document.querySelectorAll(".employee-selected");
            Array.prototype.slice.call(selectedInputs)
                .forEach( input => bodyToBeClicked.removeChild(input))
        });
    }

    if(submitNewProduct != null){
        submitNewProduct.addEventListener("click", ()=>{
            if(productNumberInput.value != "" || productCodeInput.value != "" || productNameInput.value != "" || productQuantityInput.value != "" || productPriceInput.value != ""){
                var product = {
                    productNumber: productNumberInput.value,
                    productCode: productCodeInput.value,
                    productName: productNameInput.value,
                    productQuantity: productQuantityInput.value,
                    productPrice: productPriceInput.value
                }
                products.insert(product, function(err, docs){
                    console.log(docs);
                    window.location.reload();
                });
            }else{
                console.log("GRESKA");
            }
        });
    }

    
    
    var mapOfProductsBetweenDates=new Map();
    

    if(showManagerPopis != null){
        showManagerPopis.addEventListener("click", () => {
            if(dateFromInput.value!=""&&timeFromInput.value!=""&&dateToInput.value!=""&&timeToInput.value!=""&&codeInput.value!=""){
                console.log(codeInput.value);

                var dateFrom = convertFromStringToDateFrom(dateFromInput.value, timeFromInput.value);
                var dateTo=convertFromStringToDateTo(dateToInput.value, timeToInput.value);
                
                sostojbi.find({}, function(err, docs){
                    if(!docs[0]){
                        console.log("GRESKA");
                    }else{
                        for(var i=0;i<docs.length;i++){
                            var tmpDate=convertDateEnded(docs[i].dateEnded);
                            if(tmpDate.getTime()>=dateFrom.getTime()&&tmpDate.getTime()<=dateTo.getTime()){
                                var listOfProducts=docs[i].tableProducts;
                                if (listOfProducts.length > 0){
                                    for(var j=0;j<listOfProducts.length;j++){
                                        if(mapOfProductsBetweenDates.has(listOfProducts[j]['ime'])){
                                            var currentKolicinaAmount=mapOfProductsBetweenDates.get(listOfProducts[j]['ime']);
                                            mapOfProductsBetweenDates.set(listOfProducts[j]['ime'], currentKolicinaAmount+parseInt(listOfProducts[j]['kolicina']));
                                        }else{
                                            mapOfProductsBetweenDates.set(listOfProducts[j]['ime'], parseInt(listOfProducts[j]['kolicina']))
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
                
                printPopisTable(mapOfProductsBetweenDates);
            }else{
                console.log("GRESKA");
            }
        })
    }

    var mapOfProductsForSostojbi=new Map();

    if(btnShowSold != null){
        btnShowSold.addEventListener("click", function(){
            if(inputSostojbiDateFrom.value!=""&&inputSostojbiTimeFrom.value!=""&&inputSostojbiDateTo.value!=""&&inputSostojbiTimeTo.value!=""&&inputSostojbiCode.value!=""){

                var dateFrom = convertFromStringToDateFrom(inputSostojbiDateFrom.value, inputSostojbiTimeFrom.value);
                var dateTo=convertFromStringToDateTo(inputSostojbiDateTo.value, inputSostojbiTimeTo.value);
                
                sostojbi.find({}, function(err, docs){
                    if(!docs[0]){
                        console.log("GRESKA");
                    }else{
                        for(var i=0;i<docs.length;i++){
                            var tmpDate=convertDateEnded(docs[i].dateEnded);
                            if(tmpDate.getTime()>=dateFrom.getTime()&&tmpDate.getTime()<=dateTo.getTime()){
                                var listOfProducts=docs[i].tableProducts;
                                if (listOfProducts.length > 0){
                                    for(var j=0;j<listOfProducts.length;j++){
                                        if(mapOfProductsForSostojbi.has(listOfProducts[j]['ime'])){
                                            var currentKolicinaAmount=mapOfProductsForSostojbi.get(listOfProducts[j]['ime']);
                                            mapOfProductsForSostojbi.set(listOfProducts[j]['ime'], currentKolicinaAmount+parseInt(listOfProducts[j]['kolicina']));
                                        }else{
                                            mapOfProductsForSostojbi.set(listOfProducts[j]['ime'], parseInt(listOfProducts[j]['kolicina']))
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
                printSostojbiShowSold(mapOfProductsForSostojbi);
            }else{
                console.log("GRESKA");
            }
        })
    }
   
});


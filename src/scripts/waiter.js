var Datastore = require('nedb');

var employees=new Datastore({filename: './databases/employees.db', autoload: true, corruptAlertThreshold: 1});
var products=new Datastore({filename: "./databases/products.db", autoload: true, corruptAlertThreshold: 1});
var tables=new Datastore({filename: "./databases/tables.db", autoload: true, corruptAlertThreshold: 1});
var sostojbi = new Datastore({filename: "./databases/sostojbi.db", autoload: true, corruptAlertThreshold: 1})

var currentTableName;

function resetProductsTable(){
    $("#productsTable tbody").empty();
    products.find({}, function(err, docs){
        console.log(docs);
        console.log(currentTableName);
        if(!docs[0]){
            console.log("GRESKA");
        }else{
            for(var i=0;i<docs.length;i++){
                var redenBroj = docs[i].productNumber;
                var ime = docs[i].productName;
                var cena = docs[i].productPrice;
                var tbodyRef = document.getElementById('productsTable').getElementsByTagName('tbody')[0];
                var newRow = tbodyRef.insertRow(tbodyRef.rows.length);
                newRow.innerHTML = "<tr><td>"+redenBroj+"</td><td>"+ime+"</td><td>"+cena+"</td></tr>";
            }
        }
    });
}

function resetTableOrder(){
    $("#tableOrder tbody").empty();
    $('#tableOrder tr:gt(0)').remove();

    currentTableName=localStorage.getItem("tableName");
    tables.find({tableName: currentTableName}, function(err, docs){
        if(!docs[0]){
            console.log("GRESKA");
        }else{
            var theadRef = document.getElementById('tableOrder').getElementsByTagName('thead')[0];
            var newRow = theadRef.insertRow(theadRef.rows.length);
            newRow.innerHTML = "<tr><td colspan=3>"+currentTableName+"</td></tr>";
            
            var productsInOrder=docs[0].tableProducts;
            if(productsInOrder.length > 0){
                for(var i = 0;i<productsInOrder.length;i++){
                    var imee = productsInOrder[i].ime;
                    var kolicinaa=productsInOrder[i].kolicina;
                    var cenaa=productsInOrder[i].cena;
                    var tbodyRef = document.getElementById('tableOrder').getElementsByTagName('tbody')[0];
                    var newRow = tbodyRef.insertRow(tbodyRef.rows.length);
                    newRow.innerHTML = "<tr><td>"+imee+"</td><td>"+kolicinaa+"</td><td>"+cenaa+"</td></tr>";

                }
            }
        }
    })
}

window.addEventListener("DOMContentLoaded", () => {
    const inputWaiterPassword=document.getElementById("waiter-password");
    const inputWaiterTable=document.getElementById("waiter-table");
    const openWaiterSale=document.getElementById("btn-sale");
    const transferProductRight=document.getElementById("btn-waiter-transfer-right");
    const btnDeleteProduct = document.getElementById("btn-waiter-delete");
    const btnWaiterPrint=document.getElementById("btn-waiter-print");

    if(openWaiterSale != null){
        openWaiterSale.addEventListener("click", () => {
            if(inputWaiterPassword.value != "" || inputWaiterTable.value != ""){
                employees.find({password: inputWaiterPassword.value}, function(err, docs){
                    if(docs[0].type=="MANAGER"||docs[0].type=="WAITER"){
                        tables.find({tableName: inputWaiterTable.value}, function(err, docs){
                            if(!docs[0]){
                                var today = new Date();
                                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                                var dateTime = date+' '+time;
                                var table={
                                    tableName: inputWaiterTable.value,
                                    tableProducts: [],
                                    waiterServing: inputWaiterPassword.value,
                                    totalAmount: 0,
                                    dateStarted: dateTime,
                                    dateEnded: ""
                                }
                                tables.insert(table, function(err, docs){
                                    console.log(docs);
                                });
                            }
                        });
                        localStorage.setItem("tableName", inputWaiterTable.value);
                        document.location.href="../html-forms/waiter-sale.html";
                    }else{
                        console.log("GRESKA");
                    }
                });
            }
        });
    }

    resetProductsTable();
    var selectedProduct;
    var bodyToBeClicked;
    bodyToBeClicked=document.getElementById("productsTable").tBodies[0];
    console.log(bodyToBeClicked);        
    $(bodyToBeClicked).on('click','tr', function(){
        $(this).addClass("product-selected").siblings().removeClass("product-selected");
        selectedProduct=$(this);
        console.log(selectedProduct);
    })


    resetTableOrder();
    var selectedOrder;
    var bodyToBeClicked1;

    bodyToBeClicked1=document.getElementById("tableOrder").tBodies[0];
    console.log(bodyToBeClicked1);
    $(bodyToBeClicked1).on('click','tr', function(){
        $(this).addClass("order-selected").siblings().removeClass("order-selected");
        selectedOrder=$(this);
        console.log(selectedOrder);
    })

    

    $("#waiter-search").on('input',function(){
        var searchInput = $(this).val();
        var rows=bodyToBeClicked.rows;
        $(rows).each(function(){
            var found='false';
            $(this).each(function(){
                if($(this).text().toLowerCase().indexOf(searchInput.toLowerCase()) >= 0){
                    found='true';
                }
            });
            if(found=='true'){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
    });


    if(transferProductRight != null){
        transferProductRight.addEventListener("click", function(){
           
            var cenaToAdd=0;
            var imeToAdd;
            var totalAmountToAdd=0;
            var kolicinaToAdd=0;
            var quantityInput = document.getElementById("waiter-quant");
            if(quantityInput.value != ""){
                var selectedProductName= $(selectedProduct).find('td:nth-child(2)').html();
                products.find({productName: selectedProductName}, function(err, docs){
                    var availableProducts=docs[0].productQuantity;
                    if(!docs[0] || availableProducts < quantityInput.value){
                        console.log("GRESKA");
                    }else{
                        var kolicina = quantityInput.value;
                        kolicinaToAdd=kolicina;
                        var ime = docs[0].productName;
                        imeToAdd=ime;
                        var cena = docs[0].productPrice*kolicina;
                        cenaToAdd=cena;
                        
                        products.update({productName: selectedProductName}, {$set: {productQuantity: availableProducts-kolicina}})
                        
                    }
                });
                
            }
            currentTableName=localStorage.getItem("tableName")
            tables.find({tableName: currentTableName}, function(err, docs){
                if(!docs[0]){
                    console.log("GRESKA");
                }else{
                    var productExists=false;
                    totalAmountToAdd=docs[0].totalAmount;
                    var listToBeAdded=docs[0].tableProducts;
                    
                    for(var i=0;i<listToBeAdded.length;i++){
                        var tmpObj = {}
                        tmpObj['ime']=imeToAdd;
                        tmpObj['kolicina']=parseInt(kolicinaToAdd);
                        tmpObj['cena']=parseInt(kolicinaToAdd)*parseInt(cenaToAdd);
                        if(listToBeAdded[i]['ime']==tmpObj['ime']){
                            productExists=true;
                            listToBeAdded[i]['kolicina']=parseInt(listToBeAdded[i]['kolicina'])+parseInt(kolicinaToAdd);
                            listToBeAdded[i]['cena']=parseInt(listToBeAdded[i]['cena']+parseInt(cenaToAdd))
                        }
                    }
                    if(!productExists){
                        var tmpObj = {}
                        tmpObj['ime']=imeToAdd;
                        tmpObj['kolicina']=parseInt(kolicinaToAdd);
                        tmpObj['cena']=parseInt(kolicinaToAdd)*parseInt(cenaToAdd);
                        listToBeAdded.push(tmpObj);
                    }
                    
                    tables.update({tableName: currentTableName}, {$set: {tableProducts: listToBeAdded}}, {}, function(err, numReplaced){
                        console.log("Number replaced: "+numReplaced);

                    });
                    tables.update({tableName: currentTableName}, {$set: {totalAmount: totalAmountToAdd+cenaToAdd}});
                    resetTableOrder();
                }
            });
        });
    }

    

    if(btnDeleteProduct != null){
        btnDeleteProduct.addEventListener("click", function(){
            var cenaToRemove=0;
            var imeToRemove;
            var selectedOrderProductName= $(selectedOrder).find('td:nth-child(1)').html();
            imeToRemove=selectedOrderProductName;
            var selectedOrderQuantity=$(selectedOrder).find("td:nth-child(2)").html();
            var kolicinaToRemove = parseInt(selectedOrderQuantity);
            var selectedOrderProductPrice=$(selectedOrder).find('td:nth-child(3)').html();
            var totalAmountToRemove=parseInt(selectedOrderProductPrice);
            products.find({productName: selectedOrderProductName}, function(err, docs){
                if(!docs[0]){
                    console.log("GRESKA");
                }else{
                    cenaToRemove=docs[0].productPrice*kolicinaToRemove;
                    products.update({productName: selectedOrderProductName}, {$set: {productQuantity: docs[0].productQuantity + selectedOrderQuantity}});
                }
            });
            currentTableName=localStorage.getItem("tableName")
            tables.find({tableName: currentTableName}, function(err, docs){
                if(!docs[0]){
                    console.log("GRESKA");
                }else{
                    var totalAmountNow=docs[0].totalAmount;
                    var listToBeRemoved=docs[0].tableProducts;
                    for(var i=0;i<listToBeRemoved.length;i++){
                        var tmpObj = {}
                        tmpObj['ime']=imeToRemove;
                        tmpObj['kolicina']=parseInt(kolicinaToRemove);
                        tmpObj['cena']=parseInt(cenaToRemove);
                        if(listToBeRemoved[i]['ime']==tmpObj['ime']){
                            listToBeRemoved[i]['kolicina']=parseInt(listToBeRemoved[i]['kolicina'])-parseInt(kolicinaToRemove);
                            listToBeRemoved[i]['cena']=parseInt(listToBeRemoved[i]['cena'])-parseInt(cenaToRemove);
                        }
                    }
                    var newList=listToBeRemoved.filter(function(p){
                        return p.kolicina>0;
                    });
                    console.log(newList)
                    tables.update({tableName: currentTableName}, {$set: {tableProducts: newList}}, function(err, numRemoved){
                        console.log("Number replaced: "+numRemoved);
                    });

                    tables.update({tableName: currentTableName}, {$set: {totalAmount: totalAmountNow-totalAmountToRemove}});
                    resetTableOrder();
                }
            });
        });
    }

    if(btnWaiterPrint != null){
        btnWaiterPrint.addEventListener("click", () => {
            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date+' '+time;
            currentTableName=localStorage.getItem("tableName")
            tables.update({tableName: currentTableName}, {$set: {dateEnded: dateTime}});
            tables.find({tableName: currentTableName}, function(err, docs){
                var sostojba={
                    tableName: docs[0].tableName,
                    tableProducts: docs[0].tableProducts,
                    waiterServing: docs[0].waiterServing,
                    totalAmount: docs[0].totalAmount,
                    dateStarted: docs[0].dateStarted,
                    dateEnded: docs[0].dateEnded
                }
                console.log(sostojba)
                sostojbi.insert(sostojba, function(err, docs){

                });
            });
            tables.remove({tableName: currentTableName}, {}, function(err, numRemoved){

            });
            document.location.href="./waiter-login-popup.html";
        });
    }

});